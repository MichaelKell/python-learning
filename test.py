#bound and unbound objects

class A:
     def __init__(self,x):
         self.x = x
     def spam(self,y):
        print(self.x, y)


a = A(1)
t = a.spam
t(2)
print(t)

s = A.spam
s(a, 1)
#print(s)